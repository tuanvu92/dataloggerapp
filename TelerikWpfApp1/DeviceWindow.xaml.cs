﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using System.Collections.ObjectModel;
using Telerik.Windows.Data;
using Telerik.Windows.Controls.ChartView;
using ModbusRTUClient;
namespace TelerikWpfApp1
{
    /// <summary>
    /// Interaction logic for DeviceWindow.xaml
    /// </summary>
    public partial class DeviceWindow : MetroWindow
    {
        private int clientPort;
        private ModbusRTUClient.ModbusRTUClient mbus = App.mbus;
        public DeviceWindow(int PortName)
        {
            InitializeComponent();
            clientPort = PortName;
            ChartSeries barSeries = this.chart.Series[0];
            barSeries.DataContext = new double[] { 20, 30, 50, 10, 60, 40, 20, 80 };
            mbus.DataReadyHandler += mbus_DataReadyHandler;
            mbus.ConnectEventHandler += mbus_ConnectEventHandler;            
        }

        private delegate void changeTextValue(object obj, string text);
        private void changeText(object obj, string text)
        {
            ((TextBox)obj).Text = text;
        }

        void mbus_ConnectEventHandler(object sender, ModbusConnectEventArgs e)
        {
            if (e.GetStatus()[clientPort])
            {
                this.Dispatcher.Invoke(new changeTextValue(changeText), txtStatusBar, "Connected");
            }
            else
            {
                this.Dispatcher.Invoke(new changeTextValue(changeText), txtStatusBar, "Disonnected");
            }
        }

        void mbus_DataReadyHandler(object sender, ModbusRTUClient.ModbusDataReadyEventArgs e)
        {
            ModbusRTUClient.ModbusData data = e.GetResult()[clientPort];

            this.Dispatcher.Invoke(new changeTextValue(changeText), txtLineAVp, String.Format("{0:0.00}", data.V1n));
            this.Dispatcher.Invoke(new changeTextValue(changeText), txtLineBVp, String.Format("{0:0.00}", data.V2n));
            this.Dispatcher.Invoke(new changeTextValue(changeText), txtLineCVp, String.Format("{0:0.00}", data.V3n));

            this.Dispatcher.Invoke(new changeTextValue(changeText), txtLineAIp, String.Format("{0:0.00}", data.I1));
            this.Dispatcher.Invoke(new changeTextValue(changeText), txtLineBIp, String.Format("{0:0.00}", data.I2));
            this.Dispatcher.Invoke(new changeTextValue(changeText), txtLineCIp, String.Format("{0:0.00}", data.I3));

            this.Dispatcher.Invoke(new changeTextValue(changeText), txtLineAcosp, String.Format("{0:0.00}", data.PF1));
            this.Dispatcher.Invoke(new changeTextValue(changeText), txtLineBcosp, String.Format("{0:0.00}", data.PF2));
            this.Dispatcher.Invoke(new changeTextValue(changeText), txtLineCcosp, String.Format("{0:0.00}", data.PF3));

            this.Dispatcher.Invoke(new changeTextValue(changeText), txtLineAKVA, String.Format("{0:0.00}", data.kVA1));
            this.Dispatcher.Invoke(new changeTextValue(changeText), txtLineBKVA, String.Format("{0:0.00}", data.kVA2));
            this.Dispatcher.Invoke(new changeTextValue(changeText), txtLineCKVA, String.Format("{0:0.00}", data.kVA3));
            this.Dispatcher.Invoke(new changeTextValue(changeText), txtTotalKVA, String.Format("{0:0.00}", data.TotalKVA));

            this.Dispatcher.Invoke(new changeTextValue(changeText), txtLineAKW, String.Format("{0:0.00}", data.kW1));
            this.Dispatcher.Invoke(new changeTextValue(changeText), txtLineBKW, String.Format("{0:0.00}", data.kW2));
            this.Dispatcher.Invoke(new changeTextValue(changeText), txtLineCKW, String.Format("{0:0.00}", data.kW3));
            this.Dispatcher.Invoke(new changeTextValue(changeText), txtTotalKW, String.Format("{0:0.00}", data.TotalKW));
        }        
    }

    
    
    public class SimpleViewModel : DependencyObject
    {
        Random r = new Random();
        /// <summary>
        /// Identifies the <see cref="Data"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty DataProperty = DependencyProperty.Register("Data",
            typeof(ObservableCollection<SalesInfo>),
            typeof(SimpleViewModel),
            new PropertyMetadata(null));

        public RadObservableCollection<SalesInfo> Data
        {
            get
            {
                return (RadObservableCollection<SalesInfo>)this.GetValue(DataProperty);
            }
            set
            {
                this.SetValue(DataProperty, value);
            }
        }

        public SimpleViewModel()
        {
            var data = new RadObservableCollection<SalesInfo>();

            DateTime startDate = new DateTime(2013, 5, 1);

            for (int i = 0; i < 20; i += 1)
            {
                data.Add(new SalesInfo() { Time = startDate.AddDays(i), Value = i });
            }

            this.Data = data;
        }
    }

    public class SalesInfo
    {
        public DateTime Time { get; set; }
        public int Value { get; set; }
    }
}
