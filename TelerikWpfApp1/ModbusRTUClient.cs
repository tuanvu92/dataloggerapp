﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Timers;

namespace ModbusRTUClient
{
    public delegate void ModbusDataReadyEventHandler(object sender, ModbusDataReadyEventArgs e);
    public delegate void ModbusClientConnectEventHandler(object sender, ModbusConnectEventArgs e);

    public class ModbusConnectEventArgs : EventArgs
    {
        private Dictionary<int, bool> _status;
        public ModbusConnectEventArgs(Dictionary<int,bool> status)
        {
            this._status = status;
        }
        public Dictionary<int, bool> GetStatus()
        {
            return this._status;
        }
    }

    public class ModbusDataReadyEventArgs : EventArgs
    {
        public Dictionary<int, ModbusData> _data;
        public ModbusDataReadyEventArgs(Dictionary<int, ModbusData> data)
        {
            this._data = data;
        }

        public Dictionary<int, ModbusData> GetResult()
        {
            return this._data;
        }
    }

    public class ModbusSocket
    {
        public IPAddress IPaddr { get; set; }
        public int PortName { get; set; }
    }

    public class ModbusData
    {
        public float V1n;
        public float V2n;
        public float V3n;
        public float VAvrLN;
        public float V12;
        public float V23;
        public float V31;
        public float VAvrLL;
        public float I1;
        public float I2;
        public float I3;
        public float IAvr;
        public float kW1;
        public float kW2;
        public float kW3;
        public float kVA1;
        public float kVA2;
        public float kVA3;
        public float PF1;
        public float PF2;
        public float PF3;
        public float PFAvr;
        public float Freq;
        public float kWh;
        public float kVAr1;
        public float kVAr2;
        public float kVAr3;
        public float TotalKW;
        public float TotalKVA;
        public float TotalKVAr;
    }

    public class ModbusRTUClient
    {
        public event ModbusDataReadyEventHandler DataReadyHandler;
        public event ModbusClientConnectEventHandler ConnectEventHandler;

        public static int NumOfClient = 3;
        static Boolean[] flag_done = new Boolean[NumOfClient];
        static Boolean[] flag_request = new Boolean[NumOfClient];
        public static Boolean[] isConnected = new Boolean[NumOfClient];
        static Socket[] socket = new Socket[NumOfClient];
        static TcpListener[] Server = new TcpListener[NumOfClient];
        static Thread[] thread = new Thread[NumOfClient];
        static int base_port = 8600;
        private static System.Timers.Timer aTimer;
        public int[] port = new int[NumOfClient];

        ModbusData[] mbusData = new ModbusData[NumOfClient];

        public static Dictionary<int, ModbusData> data = new Dictionary<int,ModbusData>();
        public static Dictionary<int, bool> ConnectStatus = new Dictionary<int,bool>();

        public ModbusRTUClient()
        {   
            IPHostEntry host;
            string localIP = "?";
            for (int i = 0; i < NumOfClient; i++)
            {
                data.Add(base_port + i, new ModbusData());
                ConnectStatus.Add(base_port + i, false);
                port[i] = base_port + i;
            }

            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                }
            }

            IPAddress LCIPaddr = IPAddress.Parse(localIP);            
            ModbusSocket ModbusSocket1 = new ModbusSocket() { IPaddr = LCIPaddr, PortName = 8600 };            
            ThreadPool.QueueUserWorkItem(connectClientThread, ModbusSocket1);

            ModbusSocket ModbusSocket2 = new ModbusSocket() { IPaddr = LCIPaddr, PortName = 8601 };
            ThreadPool.QueueUserWorkItem(connectClientThread, ModbusSocket2);

            ModbusSocket ModbusSocket3 = new ModbusSocket() { IPaddr = LCIPaddr, PortName = 8602 };
            ThreadPool.QueueUserWorkItem(connectClientThread, ModbusSocket3);

            for (byte i = 0; i < NumOfClient; i++)
            {
                flag_done[i] = true;
            }

            // Create a timer with a 5 second interval.
            aTimer = new System.Timers.Timer(5000);
            // Hook up the Elapsed event for the timer. 
            aTimer.AutoReset = true;
            aTimer.Elapsed += OnTimedEvent;
            aTimer.Enabled = true;

            for (byte i = 0; i < NumOfClient; i++)
            {
                flag_request[i] = false;
                //thread[i] = new Thread(new ParameterizedThreadStart(Request_client));
                //thread[i].Start(i+base_port);
            }
            ThreadPool.QueueUserWorkItem(requestClientData, base_port + 0);
            ThreadPool.QueueUserWorkItem(requestClientData, base_port + 1);
            ThreadPool.QueueUserWorkItem(requestClientData, base_port + 2);
        }

        static void connectClientThread(object obj)
        {
            ModbusSocket sock = (ModbusSocket)obj;
            while (true)
            {
                if (ConnectStatus[sock.PortName] == false)
                {
                    //TcpListener Server = new TcpListener(sock.IPaddr, sock.PortName);
                    Server[sock.PortName - base_port] = new TcpListener(sock.IPaddr, sock.PortName);
                    Server[sock.PortName - base_port].Start();
                    socket[sock.PortName - base_port] = Server[sock.PortName - base_port].AcceptSocket();
                    ConnectStatus[sock.PortName] = true;
                }
                Thread.Sleep(500);
            }
        }

        private void requestClientData(object obj)
        {
            int PortName = (int)obj;
            ModbusDataReadyEventArgs data_event = new ModbusDataReadyEventArgs(data);
            while (true)
            {
                if (flag_request[PortName - base_port] == true)
                {

                    flag_done[PortName - base_port] = false;
                    Thread.Sleep(500);
                    byte[] msg = new byte[8];
                    msg = MB_HeaderR(0x02, 0x04, 0x000, 30 * 2);
                    socket[PortName - base_port].Send(msg);
                    ModbusData result = new ModbusData();
                    byte[] data_recei_temp = new byte[200];    // 3 value *2 register * 2 byte
                    try
                    {
                        socket[PortName - base_port].Receive(data_recei_temp);
                        flag_done[PortName - base_port] = true;
                        // [0]  [1]  [2]        //slave ID  //function code  //byte count = 2*N
                        // [3]  [4]  [5]  [6]   //  data1
                        // [7]  [8]  [9]  [10]  //  data2
                        // [11] [12] [13] [14]  //  data3

                        if ((data_recei_temp[0] < 28) && (data_recei_temp[1] == 0x04) && (data_recei_temp[2] == 120)) //125
                        {
                            byte[] data_recei = new byte[125];
                            for (byte i = 0; i < 125; i++)
                            {
                                data_recei[i] = data_recei_temp[i];
                            }
                            int CRC_datarei = genCRC(data_recei);
                            byte CRC_H = 0, CRC_L = 0;
                            CRC_H = (byte)(CRC_datarei & 0x00FF);
                            CRC_L = (byte)(CRC_datarei >> 8);

                            if ((CRC_H == data_recei[123]) && (CRC_L == data_recei[124]))
                            {
                                mbusData[PortName - base_port] = showResult(data_recei);
                                data[PortName] = showResult(data_recei);
                                OnDataReady(data_event);
                            }
                            else
                            {
                                Console.WriteLine("data recive error");
                                Console.WriteLine("data recive error");
                                Console.WriteLine("data recive error");
                            }
                        }
                        Thread.Sleep(500);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Time out connection TCP");
                    }
                    flag_request[PortName - base_port] = false;
                }
            }
        }

        static int genCRC(byte[] msg)
        {
            int CRC = 0xFFFF;
            byte tmp;
            for (int i = 0; i < msg.Length - 2; i++)
            {
                tmp = (byte)(msg[i] ^ (byte)(CRC & 0x00FF));
                CRC = (CRC & 0xFF00) | tmp;
                for (int j = 0; j < 8; j++)
                {
                    byte LastBit = (byte)(CRC & 0x0001);
                    CRC = (CRC >> 1) & 0x7FFF;
                    if (LastBit == 1)
                        CRC ^= 0xA001;
                }
            }
            return CRC;
        }

        public static byte[] MB_HeaderR(byte SAdd, byte Fn, int Add, int Num)
        {
            byte[] msg = new byte[8];
            msg[0] = (byte)(SAdd);
            msg[1] = (byte)(Fn);
            msg[2] = (byte)(Add >> 8);
            msg[3] = (byte)(Add & 0x00FF);
            msg[4] = (byte)(Num >> 8);
            msg[5] = (byte)(Num & 0x00FF);
            int CRC = genCRC(msg);
            msg[6] = (byte)(CRC & 0x00FF);
            msg[7] = (byte)(CRC >> 8);
            return msg;
        }

        public static float floatConversion(byte[] bytes)
        {
            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes); // Convert big endian to little endian
            }
            byte[] tmp = new byte[2];
            tmp[0] = bytes[0];
            tmp[1] = bytes[1];

            bytes[0] = bytes[2];
            bytes[1] = bytes[3];

            bytes[2] = tmp[0];
            bytes[3] = tmp[1];

            float myFloat = BitConverter.ToSingle(bytes, 0);
            return myFloat;
        }

        public static float takeResult(byte[] data_r, byte pos)
        {
            byte[] temp_data = new byte[4];
            temp_data[0] = data_r[4 * pos + 3];      //  data1
            temp_data[1] = data_r[4 * pos + 4];      //  data1
            temp_data[2] = data_r[4 * pos + 5];      //  data1
            temp_data[3] = data_r[4 * pos + 6];      //  data1
            return floatConversion(temp_data);
        }

        public static ModbusData showResult(byte[] data_r)
        {
            ModbusData res_temp = new ModbusData();
            byte cnt_data = 0;
            res_temp.V1n = takeResult(data_r, cnt_data++);
            res_temp.V2n = takeResult(data_r, cnt_data++);
            res_temp.V3n = takeResult(data_r, cnt_data++);
            res_temp.VAvrLN = takeResult(data_r, cnt_data++);
            res_temp.V12 = takeResult(data_r, cnt_data++);
            res_temp.V23 = takeResult(data_r, cnt_data++);
            res_temp.V31 = takeResult(data_r, cnt_data++);
            res_temp.VAvrLL = takeResult(data_r, cnt_data++);
            res_temp.I1 = takeResult(data_r, cnt_data++);
            res_temp.I2 = takeResult(data_r, cnt_data++);
            res_temp.I3 = takeResult(data_r, cnt_data++);
            res_temp.IAvr = takeResult(data_r, cnt_data++);
            res_temp.kW1 = takeResult(data_r, cnt_data++);
            res_temp.kW2 = takeResult(data_r, cnt_data++);
            res_temp.kW3 = takeResult(data_r, cnt_data++);
            res_temp.kVA1 = takeResult(data_r, cnt_data++);
            res_temp.kVA2 = takeResult(data_r, cnt_data++);
            res_temp.kVA3 = takeResult(data_r, cnt_data++);
            res_temp.PF1 = takeResult(data_r, cnt_data++);
            res_temp.PF2 = takeResult(data_r, cnt_data++);
            res_temp.PF3 = takeResult(data_r, cnt_data++);
            res_temp.PFAvr = takeResult(data_r, cnt_data++);
            res_temp.Freq = takeResult(data_r, cnt_data++);
            res_temp.kWh = takeResult(data_r, cnt_data++);
            res_temp.kVAr1 = takeResult(data_r, cnt_data++);
            res_temp.kVAr2 = takeResult(data_r, cnt_data++);
            res_temp.kVAr3 = takeResult(data_r, cnt_data++);
            res_temp.TotalKW = takeResult(data_r, cnt_data++);
            res_temp.TotalKVA = takeResult(data_r, cnt_data++);
            res_temp.TotalKVAr = takeResult(data_r, cnt_data++);
            return res_temp;
        }

        private void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            ModbusConnectEventArgs connect_event = new ModbusConnectEventArgs(ConnectStatus);
            
            for (byte i = 0; i < NumOfClient; i++)
            {
                // if connected -> send request
                if (ConnectStatus[port[i]] == true)
                {   
                    if (flag_done[i] == true){
                        flag_request[i] = true;
                        requestClientData(i + base_port);
                        
                    }                        
                    else
                    {
                        flag_request[i] = false;
                        flag_done[i] = true;
                        Server[i].Stop();
                        ConnectStatus[port[i]] = false;                        
                        OnConnectEvent(connect_event);
                        Console.WriteLine("Client {0:D} disconnected...", i + 1);
                    }
                }
            }
            
        }

        protected virtual void OnDataReady(ModbusDataReadyEventArgs e)
        {
            if (DataReadyHandler != null)
            {
                DataReadyHandler(this, e);
            }
        }
        protected virtual void OnConnectEvent(ModbusConnectEventArgs e)
        {
            if (ConnectEventHandler != null)
            {
                ConnectEventHandler(this, e);
            }
        }        
    }
}
