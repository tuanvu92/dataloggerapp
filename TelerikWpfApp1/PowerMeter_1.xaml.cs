﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TelerikWpfApp1
{
    /// <summary>
    /// Interaction logic for PowerMeter_1.xaml
    /// </summary>
    public partial class PowerMeter_1 : UserControl
    {
        private Window deviceWindow;
        private int portName;
        public static DependencyProperty DeviceIDProperty = DependencyProperty.Register(
            "DeviceID", typeof(string), typeof(PowerMeter_1), new PropertyMetadata((string)"?"));
        public string DeviceID
        {
            get { return (string)this.GetValue(DeviceIDProperty); }
            set 
            {
                this.SetValue(DeviceIDProperty, value);
                this.lbDeviceID.Content = this.GetValue(DeviceIDProperty);
            }
        }

        
        public static DependencyProperty PortNameProperty = DependencyProperty.Register(
            "PortName", typeof(int), typeof(PowerMeter_1), new PropertyMetadata((int)0));
        public int PortName
        {
            get { return (int)this.GetValue(PortNameProperty); }
            set
            {
                this.SetValue(PortNameProperty, value);
                this.portName = (int)this.GetValue(PortNameProperty);
            }
        }
        
        public PowerMeter_1()
        {
            InitializeComponent();
            lbDeviceID.Content = (string)this.GetValue(DeviceIDProperty);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (deviceWindow == null)
            {
                deviceWindow = new DeviceWindow(portName);
                deviceWindow.Closed += (o, args) => deviceWindow = null;
            }
            if (deviceWindow.IsVisible)
            {
                deviceWindow.Hide();                
            }
            else
            {
                deviceWindow.Show();
                //deviceWindow.Title = "Device " + this.DeviceID;
                deviceWindow.Title = this.DeviceID;
                
            }
        }        
    }
}