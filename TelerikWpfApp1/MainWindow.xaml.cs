﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using ModbusRTUClient;

namespace TelerikWpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// \
    public partial class MainWindow : MetroWindow
    {
        ModbusRTUClient.ModbusRTUClient mbus = App.mbus; 
        public MainWindow()
        {
            InitializeComponent();

            PW_1.PortName = mbus.port[0];
            PW_1.DeviceID = mbus.port[0].ToString();
            PW_2.PortName = mbus.port[1];
            PW_2.DeviceID = mbus.port[1].ToString();
            PW_3.PortName = mbus.port[2];
            PW_3.DeviceID = mbus.port[2].ToString();

            mbus.DataReadyHandler += new ModbusDataReadyEventHandler(UpdateDataHandler);
            mbus.ConnectEventHandler += new ModbusClientConnectEventHandler(OnConnectStatusChange);
            
            
        }

        private delegate void setActiveButtonDelegate(object obj, bool state);
        private void setActiveButton(object obj, bool state)
        {
            ((PowerMeter_1)obj).IsEnabled = state;
        }

        private delegate void updateDelegate(object sender, string text);
        private void displayText(object sender, string text)
        {
            ((TextBlock)sender).Text = text;
        }


        private void UpdateDataHandler(object obj, ModbusDataReadyEventArgs e)
        {
            this.Dispatcher.Invoke(new updateDelegate(displayText), PW_1.txtPower, String.Format("{0:0.00}", e.GetResult()[mbus.port[0]].kWh));
            this.Dispatcher.Invoke(new updateDelegate(displayText), PW_2.txtPower, String.Format("{0:0.00}", e.GetResult()[mbus.port[1]].kWh));
            this.Dispatcher.Invoke(new updateDelegate(displayText), PW_2.txtPower, String.Format("{0:0.00}", e.GetResult()[mbus.port[1]].kWh));         
        }

        private delegate void ChangeButtonStatusDelegate(object obj, SolidColorBrush b);
        private void ChangeButtonStatus(object obj, SolidColorBrush b)
        {
            ((PowerMeter_1)obj).recStatus.Fill = b;
        }

        private void OnConnectStatusChange(object obj, ModbusConnectEventArgs e)
        {                        
            if (e.GetStatus()[mbus.port[0]])
            {
                this.Dispatcher.Invoke(new ChangeButtonStatusDelegate(ChangeButtonStatus), PW_1, new SolidColorBrush(System.Windows.Media.Colors.Lime));
            }
            else
            {
                this.Dispatcher.Invoke(new ChangeButtonStatusDelegate(ChangeButtonStatus), PW_1, new SolidColorBrush(System.Windows.Media.Colors.Red));
            }

            if (e.GetStatus()[mbus.port[1]])
            {
                this.Dispatcher.Invoke(new ChangeButtonStatusDelegate(ChangeButtonStatus), PW_2, new SolidColorBrush(System.Windows.Media.Colors.Lime));
            }
            else
            {
                this.Dispatcher.Invoke(new ChangeButtonStatusDelegate(ChangeButtonStatus), PW_2, new SolidColorBrush(System.Windows.Media.Colors.Red));
            }

            if (e.GetStatus()[mbus.port[2]])
            {
                this.Dispatcher.Invoke(new ChangeButtonStatusDelegate(ChangeButtonStatus), PW_3, new SolidColorBrush(System.Windows.Media.Colors.Lime));
            }
            else
            {
                this.Dispatcher.Invoke(new ChangeButtonStatusDelegate(ChangeButtonStatus), PW_3, new SolidColorBrush(System.Windows.Media.Colors.Red));
            }             
        }

        private void PowerMeter_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}
