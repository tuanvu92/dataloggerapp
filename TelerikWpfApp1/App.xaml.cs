﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

namespace TelerikWpfApp1
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static ModbusRTUClient.ModbusRTUClient mbus = new ModbusRTUClient.ModbusRTUClient(); 
        public App()
        {
           this.InitializeComponent();
            
        }
    }
}
